		</div>
	</div>
</div>


<div class="cta-banner geo-numbers">
	<div class="container">
		<div class="inner">
			<span class="h1">Get help for your or a loved one today.</span>
			Speak to an addiction therapist for a free confidential assessment.

			<?php echo ot_get_option('cta'); ?><br>
			<span class="us-num">Toll free from USA: <?php echo do_shortcode('[phone_us]'); ?></span>
			<?php echo do_shortcode('[phone_uk]'); ?>
			<a href="<?php echo ot_get_option( 'phone_au' ); ?>" class="au-num"><?php echo do_shortcode('[phone_au]'); ?></a>
			<a href="<?php echo ot_get_option( 'phone_int' ); ?>" class="int-num"><?php echo do_shortcode('[phone_int]'); ?></a>
		</div>
	</div>
</div>

<div class="main">
	<div class="container">
		<div class="text">