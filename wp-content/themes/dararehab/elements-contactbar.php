		<?php if (!is_page('38')) { ?>
			<div class="contact-bar">
				<div class="container">
					<div class="col-lg-4 phone">
						<a href="<?php echo get_page_link(38); ?>">
							<span>Call Us Now <span>Phone</span></span>
							<span>
								<table>
								<tr>
									<td>International:</td>
									<td><a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></td>
								</tr>
								<tr>
									<td>Australia:</td>
									<td><a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></td>
								</tr>
								<tr>
								<td>United Kingdom:</td>
								<td><?php echo do_shortcode('[phone_uk]'); ?></td>
							</tr>
							<tr>
								<td>USA &amp; Canada:</td>
								<td><?php echo do_shortcode( '[phone_us]' ); ?></td>
							</tr>
									<tr>
										<td colspan="2"><span class="glyphicon glyphicon-play"></span> All Other International Phone Numbers</td>
									</tr>
								</table>
							</span>
						</a>
					</div>
					<div class="col-lg-4 email desktop">
						<a href="#form" class="fancybox">
							<span>Email Us Now <span>Email</span></span>
						</a>
					</div>
					<div class="col-lg-4 email mobile">
						<a href="/contact">
							<span>Email Us Now <span>Email</span></span>
						</a>
					</div>
					<div class="col-lg-4 skype">
						<a href="skype:dara-thailand?call">
							<span>Skype Us Now <span>Skype</span></span>
						</a>
					</div>
				</div>
			</div>
		<?php } ?>