<?php
function remove_dashboard_widgets() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

function move_dashboard_widget() {
	global $wp_meta_boxes;
	$my_widget = $wp_meta_boxes['dashboard']['normal']['core']['{dashboard_right_now}'];
	unset($wp_meta_boxes['dashboard']['normal']['core']['{dashboard_right_now}']);
	$wp_meta_boxes['dashboard']['side']['core']['{dashboard_right_now}'] = $my_widget;
}
add_action( 'wp_dashboard_setup', 'move_dashboard_widget' );
?>