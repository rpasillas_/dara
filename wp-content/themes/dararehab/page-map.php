<?php
/*
** Template Name: World Map
*/
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post();
if ( is_front_page() ) { get_template_part( 'page', 'home' ); } else { ?>
			<div class="header" style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dot.png'), url('<?php echo get_template_directory_uri(); ?><?php banner_tree_img(); ?>') no-repeat center center / cover">
				<h1><?php the_title(); ?></h1>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">
					<div class="col-lg-12 text">
						<?php if ( !is_page( '164' ) ) {
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<p class="breadcrumbs"><a href="' . home_url() . '"><span class="glyphicon glyphicon-home"></span></a>','</p>');
							}
						} ?>
						<h1><?php the_title(); ?>
						<div id="world-map" style="width: 100%; height: 450px"></div>
						<?php the_content(); ?>
						<script>
							$(function(){
								var activeColor = "#f6a84b";
								var activeRegions = {
									"AU" : activeColor,
									"BD" : activeColor,
									"BN" : activeColor,
									"KH" : activeColor,
									"CA" : activeColor,
									"HK" : activeColor,
									"IN" : activeColor,
									"ID" : activeColor,
									"JP" : activeColor,
									"MY" : activeColor,
									"PH" : activeColor,
									"SA" : activeColor,
									"SG" : activeColor,
									"ZA" : activeColor,
									"CH" : activeColor,
									"TW" : activeColor,
									"TH" : activeColor,
									"UK" : activeColor,
									"US" : activeColor
								};
								/*var middleEast = {
									"EG" : activeColor,
									"IR" : activeColor,
									"TR" : activeColor,
									"IQ" : activeColor
								};*/

								$('#world-map').vectorMap({
									map: 'world_mill_en',
									backgroundColor: '#223c62',
									//zoomStep: Math.pow(1.003, event.deltaY),
									regionStyle: {
										initial: {
											fill: '#667283'
										},
									},
									series: {										
										regions: [{
											values: activeRegions,
											attribute: 'fill'
										}]
									/*	regions: [{
											values: activeRegions,
											attribute: 'fill'
										},{
											values: middleEast
										}]*/
									},

									onRegionOver: function(e, code) {
										if (activeRegions.hasOwnProperty(code)) {
											document.body.style.cursor = 'pointer';
										}
										if (!activeRegions.hasOwnProperty(code)) {
											return false;
										}
									},
									onRegionOut: function(e, code) {
										// return to normal cursor
										document.body.style.cursor = 'default';
									},
									onRegionTipShow: function(e, el, code) {
										if (!activeRegions.hasOwnProperty(code)) {
											// the hovered region is not part of the activeRegions, don't show the label
											return false;
										}
									},
									onRegionClick: function(e, code) {
										var url = null;
										if (activeRegions.hasOwnProperty(code)) {
											if (code == "AU") { url = "australia" }
											if (code == "BD") { url = "bangladesh" }
											if (code == "BN") { url = "brunei" }
											if (code == "KH") { url = "cambodia" }
											if (code == "CA") { url = "canada" }
											if (code == "HK") { url = "hong-kong" }
											if (code == "IN") { url = "india" }
											if (code == "ID") { url = "indonesia" }
											if (code == "JP") { url = "japan" }
											if (code == "MY") { url = "malaysia" }
											if (code == "PH") { url = "philipines" }
											if (code == "SA") { url = "saudi-arabia" }
											if (code == "ZA") { url = "south-africa" }
											if (code == "CH") { url = "switzerland" }
											if (code == "TA") { url = "taiwan" }
											if (code == "TH") { url = "thailand" }
											if (code == "GB") { url = "united-kingdom" }
											if (code == "US") { url = "united-states" }
											if (code == "VN") { url = "vietnam" }
											window.location.href = "http://dararehab.com/drug-alcohol-rehab/" + url + "/";
										} else {
											return;
										}
									},
								});
							});
						</script>

					</div>
				</div>
			</div>
		</div><!-- /.wrapper -->

<?php
	}
endwhile; endif;
get_footer(); ?>