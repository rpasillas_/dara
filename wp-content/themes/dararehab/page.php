<?php get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();

	if ( is_front_page() ) { get_template_part( 'page', 'home' ); } else { ?>
			<div class="header" style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dot.png'), url('<?php echo get_template_directory_uri(); ?><?php banner_tree_img(); ?>') no-repeat center center / cover">
				<h1><?php the_title(); ?></h1>
			</div>

			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>

			<div class="main">
				<div class="container">

					<?php get_sidebar(); ?>

					<div class="col-lg-8 col-md-pull-4 text">
						<?php if ( !is_page( '164' ) ) {
						    if ( function_exists('yoast_breadcrumb') ) {
							    yoast_breadcrumb('<p class="breadcrumbs"><a href="' . home_url() . '"><span class="glyphicon glyphicon-home"></span></a>','</p>');
						    }
						} ?>
						<?php the_content(); ?>
					</div>

					<?php //get_sidebar(); ?>

				</div>
			</div>
		</div><!-- /.wrapper -->


	<?php } ?>

<?php endwhile; endif; ?>

<?php get_footer(); ?>