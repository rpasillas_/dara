<?php
	function custom_dashboard_widget() {
		echo "<code>[phone_us]: US Phone Number</code><br/><br/>
		<code>[cta_giant]Giant Call-to-Action[/cta_giant]</code><br/><br/>
		<code>[cta_large]Large Call-to-Action[/cta_large]</code><br/><br/>
		<code>[cta_medium]Midsize Call-to-Action[/cta_medium]</code><br/><br/>
		<code>[cta_small]Small-ish Call-to-Action[/cta_small]</code><br/><br/>
		<code>[cta_mini]Mini Call-to-Action[/cta_mini]</code><br/><br/>
		<code>[panel title=\"My Title\"]Panel content.[/panel]</code><br/><br/>
		<code>[panel_left title=\"My Title\"]Content.[/panel]</code><br/><br/>
		<code>[panel_right title=\"My Title\"]Content.[/panel]</code><br/><br/>
		<code>[pullquote]Pullquote content here.[/pullquote]</code><br/><br/>
		<code>[widecap caption=\"My Caption\"]&lt;img src=\"img.jpg\" /&gt;[/widecap]</code><br/><br/>
		<h3>Source Code</h3>
		<code>&lt;div class=\"vid two\"&gt;&lt;iframe src=\"first.youtube.link.here\"&gt;&lt;/iframe&gt;&lt;iframe src=\"second.youtube.link.here\"&gt;&lt;/iframe&gt;&lt;/div&gt;</code><br/><br/>
		<h3>Recommendations</h3>
		<ul style=\"list-style:disc;margin-left:30px\"><li>Don't use any of the CTAs inside panels. The panel is itself already a CTA.</li>
		<li>Keep the iframes in the \"vid two\" div always butting up against each other on one line -- no line breaks! Otherwise, they won't display properly.</li></ul>";
	}
	function add_custom_dashboard_widget() {
		wp_add_dashboard_widget('example_dashboard_widget', 'Shortcode List', 'custom_dashboard_widget', 'page');
	}
	add_action('wp_dashboard_setup', 'add_custom_dashboard_widget' );
?>