			<?php $phone = do_shortcode('[geo-phone]'); ?>

			<div class="hero">
				<div class="callbacks_container">
					<ul class="rslides">
						<li style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dotnone.png'), url('<?php echo ot_get_option( 'slider_image_0' ); ?>') no-repeat center center / cover;">
							<div class="container">
								<div class="page-header">
									<img src="<?php echo get_template_directory_uri(); ?>/img/home-slider-logo.png" alt="DaraRehab.com" />
								</div>
								<p class="lead"><span style="font-size:22px;"><?php echo ot_get_option( 'slider_text_0' ); ?><br/>
								<?php echo $phone; ?></span>
								<br/><br/>
								<a href="http://dararehab.com/contact/" type="button" class="button slider fancybox inline">Contact Us</a></p>
							</div>
						</li>

						<li style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dotnone.png'), url('<?php echo ot_get_option( 'slider_image_1' ); ?>') no-repeat center center / cover;">
							<div class="container">
								<div class="page-header">
									<img src="<?php echo get_template_directory_uri(); ?>/img/home-slider-logo.png" alt="DaraRehab.com" />
								</div>
								<p class="lead"><?php echo ot_get_option( 'slider_text_1' ); ?> <?php echo $phone; ?>
								<br/><br/>
								<a href="#form" type="button" class="button slider fancybox inline">Contact Us Today</a></p>
							</div>
						</li>

						<li style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dotnone.png'), url('<?php echo ot_get_option( 'slider_image_2' ); ?>') no-repeat center center / cover;">
							<div class="container">
								<div class="page-header">
									<img src="<?php echo get_template_directory_uri(); ?>/img/home-slider-logo.png" alt="DaraRehab.com" />
								</div>
								<p class="lead"><?php echo ot_get_option( 'slider_text_2' ); ?><br/><?php echo $phone; ?>
								<br/><br/>
								<a href="http://dararehab.com/contact/" type="button" class="button slider fancybox inline">Contact Us</a></p>
							</div>
						</li>

						<li style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dotnone.png'), url('<?php echo ot_get_option( 'slider_image_3' ); ?>') no-repeat center center / cover;">
							<div class="container">
								<div class="page-header">
									<img src="<?php echo get_template_directory_uri(); ?>/img/home-slider-logo.png" alt="DaraRehab.com" />
								</div>
								<p class="lead"><?php echo ot_get_option( 'slider_text_3' ); ?><br/><?php echo $phone; ?>
								<br/><br/>
								<a href="#form" type="button" class="button slider fancybox inline">Contact Us Today</a></p>
							</div>
						</li>
						<li style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dotnone.png'), url('<?php echo ot_get_option( 'slider_image_5' ); ?>') no-repeat center center / cover;">
							<div class="container">
								<div class="page-header">
									<img src="<?php echo get_template_directory_uri(); ?>/img/home-slider-logo.png" alt="DaraRehab.com" />
								</div>
								<p class="lead"><?php echo ot_get_option( 'slider_text_5' ); ?><br/><?php echo $phone; ?>
								<br/><br/>
								<a href="#form" type="button" class="button slider fancybox inline">Contact Us Today</a></p>
							</div>
						</li>

					</ul>
				</div><!-- /slider -->
			</div>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<?php get_template_part('elements', 'social'); ?>
				<div class="container">
					<div class="text">							
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="photos">
				<div class="banner">
					<div class="container">
						<span>DARA Koh Chang</span>
						<span><a href="http://dararehab.com/koh-chang-photo-gallery/">View All Photos</a></span>
					</div>
				</div>
				<div class="container">
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-01" src="<?php echo get_template_directory_uri(); ?>/img/gallery-01.png" />
						<img class="gallery-02" src="<?php echo get_template_directory_uri(); ?>/img/gallery-02.png" />
						<img class="gallery-03" src="<?php echo get_template_directory_uri(); ?>/img/gallery-03.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-04" src="<?php echo get_template_directory_uri(); ?>/img/gallery-04.png" />
						<img class="gallery-05" src="<?php echo get_template_directory_uri(); ?>/img/gallery-05.png" />
						<img class="gallery-06" src="<?php echo get_template_directory_uri(); ?>/img/gallery-06.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-07" src="<?php echo get_template_directory_uri(); ?>/img/gallery-07.png" />
						<img class="gallery-08" src="<?php echo get_template_directory_uri(); ?>/img/gallery-08.png" />
						<img class="gallery-09" src="<?php echo get_template_directory_uri(); ?>/img/gallery-09.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-10" src="<?php echo get_template_directory_uri(); ?>/img/gallery-10.png" />
						<img class="gallery-11" src="<?php echo get_template_directory_uri(); ?>/img/gallery-11.png" />
						<img class="gallery-12" src="<?php echo get_template_directory_uri(); ?>/img/gallery-12.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-13" src="<?php echo get_template_directory_uri(); ?>/img/gallery-13.png" />
						<img class="gallery-14" src="<?php echo get_template_directory_uri(); ?>/img/gallery-14.png" />
						<img class="gallery-15" src="<?php echo get_template_directory_uri(); ?>/img/gallery-15.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-16" src="<?php echo get_template_directory_uri(); ?>/img/gallery-16.png" />
						<img class="gallery-17" src="<?php echo get_template_directory_uri(); ?>/img/gallery-17.png" />
						<img class="gallery-18" src="<?php echo get_template_directory_uri(); ?>/img/gallery-18.png" />
					</div>

				</div>
				<div class="banner">
					<div class="container">
						<span>DARA Chanthaburi</span>
						<span><a href="http://dararehab.com/chanthaburi-photo-gallery/">View All Photos</a></span>
					</div>
				</div>
				<div class="container">
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-01" src="/wp-content/uploads/2014/03/gallery-1.png" />
						<img class="gallery-02" src="/wp-content/uploads/2014/03/gallery-2.png" />
						<img class="gallery-03" src="/wp-content/uploads/2014/03/gallery-3.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-04" src="/wp-content/uploads/2014/03/gallery-4.png" />
						<img class="gallery-05" src="/wp-content/uploads/2014/03/gallery-5.png" />
						<img class="gallery-06" src="/wp-content/uploads/2014/03/gallery-6.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-07" src="/wp-content/uploads/2014/03/gallery-7.png" />
						<img class="gallery-08" src="/wp-content/uploads/2014/03/gallery-8.png" />
						<img class="gallery-09" src="/wp-content/uploads/2014/03/gallery-9.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-10" src="/wp-content/uploads/2014/03/gallery-101.png" />
						<img class="gallery-11" src="/wp-content/uploads/2014/03/gallery-111.png" />
						<img class="gallery-12" src="/wp-content/uploads/2014/03/gallery-121.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-13" src="/wp-content/uploads/2014/03/gallery-131.png" />
						<img class="gallery-14" src="/wp-content/uploads/2014/03/gallery-141.png" />
						<img class="gallery-15" src="/wp-content/uploads/2014/03/gallery-151.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-16" src="/wp-content/uploads/2014/03/gallery-161.png" />
						<img class="gallery-17" src="/wp-content/uploads/2014/03/gallery-171.png" />
						<img class="gallery-18" src="/wp-content/uploads/2014/03/gallery-181.png" />
					</div>
				</div>
			</div><!-- /Photo Gallery 2 -->

			<div class="map">
				<div class="container">
					<div class="callout">
						<span>Map</span>
						<p>Asia’s first and leading international destination for drug and alcohol rehabilitation, DARA Drug &amp; Alcohol Rehab Asia, operates two world-class centers in Thailand. </p>
					</div>
					<ul class="tacks">
						<li>
							<a href="<?php echo get_page_link(49); ?>">Chanthaburi</a>
							<div>
								<div>
									<span>Chanthaburi</span>
									<span>"City of the Moon"</span>
									<a href="<?php echo get_page_link(49); ?>" class="button">Learn More</a>
								</div>
							</div>
						</li>
						<li>
							<a href="<?php echo get_page_link(48); ?>">Koh Chang</a>
							<div>
								<div>
									<span>Koh Chang</span>
									<span>"Elephant Island"</span>
									<a href="<?php echo get_page_link(48); ?>" class="button">Learn More</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div><!-- /Map -->

			<div class="content">
				<div class="container">
					<?php echo get_post_meta($post->ID, 'secondary', true); ?>
				</div>
			</div><!-- /Content -->
		</div><!-- /.wrapper -->