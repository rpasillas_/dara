<?php
//Walker
include_once( 'inc/nav_walker_menu.php' );


//-----[ Theme-Specified ]-----\\
	add_action( 'after_setup_theme', 'dararehab_setup' );
	function dararehab_setup() {
		load_theme_textdomain( 'dararehab', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 640;
		register_nav_menus(
			array(
				'main' => __( 'Header' ),
				'footer-left' => __( 'Footer: Left' ),
				'footer-center' => __( 'Footer: Center' )
			)
		);
	}

	add_action( 'wp_enqueue_scripts', 'dararehab_load_scripts' );
	function dararehab_load_scripts() {
		wp_enqueue_script( 'jquery' );
	}

	add_action( 'comment_form_before', 'dararehab_enqueue_comment_reply_script' );
	function dararehab_enqueue_comment_reply_script() {
		if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
	}

	add_filter( 'the_title', 'dararehab_title' );
	function dararehab_title( $title ) {
		if ( $title == '' ) {
			return '&rarr;';
		} else {
			return $title;
		}
	}

	add_filter( 'wp_title', 'dararehab_filter_wp_title' );
	function dararehab_filter_wp_title( $title ) {
		return $title . esc_attr( get_bloginfo( 'name' ) );
	}

	add_action( 'widgets_init', 'dararehab_widgets_init' );
	function dararehab_widgets_init() {
		register_sidebar( array (
			'name' => __( 'Interior Sidebar', 'dararehab' ),
			'id' => 'interior',
			'before_title' => '<h3 class="headline">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array (
			'name' => __( 'Blog Sidebar', 'dararehab' ),
			'id' => 'blog',
			'before_title' => '<h3 class="headline">',
			'after_title' => '</h3>',
		) );
	}

	function dararehab_custom_pings( $comment )	{
		$GLOBALS['comment'] = $comment;
		?><li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li><?php
	}

	add_filter( 'get_comments_number', 'dararehab_comments_number' );
	function dararehab_comments_number( $count ) {
		if ( !is_admin() ) {
			global $id;
			$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
			return count( $comments_by_type['comment'] );
		} else {
			return $count;
		}
	}

	// Apply Categories & Tags to Attachments
	function wptp_add_to_attachments() {
		register_taxonomy_for_object_type( 'category', 'attachment' );
		register_taxonomy_for_object_type( 'post_tag', 'attachment' );
	}
	add_action( 'init' , 'wptp_add_to_attachments' );


//-----[ Evaero-Specified ]-----\\

//-- Remove wp_head Filters --\\
	add_action('get_header', 'filter_head');
	function filter_head() {
		remove_action('wp_head', '_admin_bar_bump_cb');
	}

//-- Shortcodes --\\
	add_filter('the_content', 'do_shortcode');
	add_filter('post_content', 'do_shortcode');
	add_filter('widget_text', 'do_shortcode');
	include('elements-shortcodes.php');

//-- Custom Dashboard Widget --\\
	include('elements-cheatsheet.php');

//-- Lineage for Page Banners --\\
	include('inc/banner-tree.php');

//-- Change Excerpt & "More" Link --\\
	function custom_excerpt_length( $length ) {
		return 45;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	function new_excerpt_more($more) {
		global $post;
		return '... <a class="read-more" href="'. get_permalink($post->ID) . '"><span class="glyphicon glyphicon-arrow-right"></span></a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');

//-- Enable Blog Pagination --\\
	include('elements-pagenav.php');

//-- Add Options Tree --\\
	add_filter( 'ot_show_pages', '__return_false' );
	add_filter( 'ot_show_new_layout', '__return_false' );
	add_filter( 'ot_theme_mode', '__return_true' );
	include_once( 'option-tree/ot-loader.php' );
	include_once( 'theme-options.php' );
	require_once( 'dashboard.php' );

 //----- Zen Desk Widget for Admins
    if ( function_exists( 'the_zendesk_webwidget' ) ) {
        $user = wp_get_current_user();

        if( user_can($user, 'administrator') ) add_action('wp_footer', 'the_zendesk_webwidget');  

    }