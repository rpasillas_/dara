<div class="entry-header">
	<span class="cat-links"><span class="glyphicon glyphicon-folder-open"></span> <?php the_category( ', ' ); ?></span>
	<?php
		if (has_tag()) { ?>
			<span class="tag-links"><span class="glyphicon glyphicon-tags"></span> <?php the_tags(); ?></span>
		<?php }
	?>
	<?php
		$comno = get_comments_number();
		if ( comments_open() ) {
			echo '<span class="glyphicon glyphicon-comment"></span> <span class="comments-link"><a href="' . get_comments_link() . '">' . $comno;
			if ( $comno == 1 ) {
				echo sprintf( __( ' Comment', 'dararehab' ) ) . '</a></span>';
			} else {
				echo sprintf( __( ' Comments', 'dararehab' ) ) . '</a></span>';
			}
	 	}
		echo '</a></span>';
	?>
</div>