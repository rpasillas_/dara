<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<html lang="en">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no">

		<title><?php wp_title( ' | ', true, 'right' ); ?></title>

		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">
		<?php if(is_front_page() || is_page('1357')) { ?><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/home.css"><?php } ?>
		<?php if(is_page_template('page-map.php')) : ?><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery-jvectormap-2.0.1.css"><?php endif; ?>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery.fancybox.pack.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/lib/js/modernizr.custom.min.js"></script>
		<?php if(is_front_page() || is_page('1357')) { ?>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery.cycle2.min.js"></script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/responsiveslides.min.js"></script>
			<script type="text/javascript">
				$(function () {
					$(".rslides").responsiveSlides({
						auto: true,
						pager: false,
						nav: true,
						speed: 500,
						timeout: 6000,
						namespace: "callbacks"
					});
				});
			</script>
			<script type="text/javascript">
				$('.cycle-overlay').show();
				$('.cycle-slideshow').mouseenter(function(){
					$('.cycle-overlay').show();
				});
			</script>
		<?php } ?>
		<?php if ( is_page_template( 'page-map.php' ) ) : ?>
			<script src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery-jvectormap-2.0.1.min.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery-jvectormap-world-mill-en.js"></script>
		<?php endif; ?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.fancybox').fancybox({
					fitToView	: false,
					openEffect	: 'fade',
					closeEffect	: 'fade',
					centerOnScroll : 'true',
					scrollOutside : 'true'
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function (){
				$('iframe').each(function(){
					var url = $(this).attr("src");

					if(url !== 'javascript:false'){
						$(this).attr("src",url+"?wmode=transparent");
					}
					
				});
			});
		</script>
		<script>
			$(document).ready(function(){
				//make mobile menu sub-nav open and close
				$('.navbar-toggle').on('click', function(){						
					var closeAll= $('#phone-mobile').find('.numbers').hasClass('on');

					if( closeAll== true ){
						console.log('phone numbers triggered');
						//$('#phone-mobile').find('.trigger').click();
						$('.numbers').toggleClass('on');
						$('#phone-mobile').find('.trigger').toggleClass('on');
					}
				});

				$('#phone-mobile').find('.trigger').on('click', function(){
					var el = $(this);
					el.toggleClass('on');
					el.siblings('.numbers').toggleClass('on');

					var closeAll = $('.navbar-collapse').hasClass('in');

					if( closeAll == true ){
						console.log('nav triggered');
						$('.navbar-header').find('button').toggleClass('collapsed');
						$('.navbar-collapse').toggleClass('in');
					}
					
				});
				$('.sub-trigger').on('click', function(){
					var el = $(this);
					el.toggleClass('on');
					el.siblings('.sub-menu').toggleClass('on');
				});
			});
		</script>
		<?php echo ot_get_option( 'google_analytics_code' ); ?>
		<?php echo ot_get_option( 'google_remarketing_code' ); ?>
		<?php echo ot_get_option( 'infinity_code' ); ?>

		<?php wp_head(); ?>

	</head>

	<?php if(is_front_page() || is_page('1357')) { ?>
		<body id="home">
	<?php } else { ?>
		<body>
	<?php } ?>

		<div id="wrap">

			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<a href="<?php echo home_url(); ?>" id="logo"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/header-logo.png" alt="DARARehab.com" /></a>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="collapse navbar-collapse">
						<?php wp_nav_menu( array( 'theme_location' => 'main', 'container' => '', 'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>', 'walker' => new nav_walker_menu ) ); ?>
					</div><!-- /.nav-collapse -->

					<div id="phone-mobile">
						<button class="trigger"></button>
						<div class="numbers">
							<ul>
							<?php $country = do_shortcode('[geoip-country]'); ?>

								<?php if($country=='US'){ ?>
									<li><span class="numbers-wrap">Toll-free from USA &amp; Canada: <?php echo do_shortcode( '[phone_us]' ); ?></span></li>

									<li><span class="numbers-wrap">International: <a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></span></li>
							
									<li><span class="numbers-wrap">Toll-free from Australia: <a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></span></li>
								
									<li><span class="numbers-wrap">Toll-free from United Kingdom: <?php echo do_shortcode( '[phone_uk]' ); ?></span></li>
								
									

								<?php }else if($country == 'GB'){ ?>
									<li><span class="numbers-wrap">Toll-free from United Kingdom: <?php echo do_shortcode( '[phone_uk]' ); ?></span></li>
									
									<li><span class="numbers-wrap">International: <a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></span></li>
							
									<li><span class="numbers-wrap">Toll-free from Australia: <a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></span></li>					
									<li><span class="numbers-wrap">Toll-free from USA &amp; Canada: <?php echo do_shortcode( '[phone_us]' ); ?></span></li>



								<?php }else if($country == 'AU'){ ?>
									<li><span class="numbers-wrap">Toll-free from Australia: <a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></span></li>

									<li><span class="numbers-wrap">International: <a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></span></li>
								
									<li><span class="numbers-wrap">Toll-free from United Kingdom: <?php echo do_shortcode( '[phone_uk]' ); ?></span></li>
								
									<li><span class="numbers-wrap">Toll-free from USA &amp; Canada: <?php echo do_shortcode( '[phone_us]' ); ?></span></li>


								<?php }else{ ?>
									<li><span class="numbers-wrap">International: <a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></span></li>
							
									<li><span class="numbers-wrap">Toll-free from Australia: <a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></span></li>
								
									<li><span class="numbers-wrap">Toll-free from United Kingdom: <?php echo do_shortcode( '[phone_uk]' ); ?></span></li>
								
									<li><span class="numbers-wrap">Toll-free from USA &amp; Canada: <?php echo do_shortcode( '[phone_us]' ); ?></span></li>		
								<?php } ?>


								



							</ul>
						</div>
					</div>

				</div>
			</div><!-- /.navbar -->