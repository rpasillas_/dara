<?php get_header(); ?>
<?php global $post; ?>
			<div class="header">
			    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				    <span class="page-title">Attachment: <?php the_title(); ?></span>
				<?php endwhile; endif; ?>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">
					<div class="col-lg-8 text">
					    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <?php if ( wp_attachment_is_image( $post->ID ) ) : $att_image = wp_get_attachment_image_src( $post->ID, "large" ); ?>
								<p class="attachment"><a href="<?php echo wp_get_attachment_url( $post->ID ); ?>" title="<?php the_title(); ?>" rel="attachment"><img src="<?php echo $att_image[0]; ?>" width="<?php echo $att_image[1]; ?>" height="<?php echo $att_image[2]; ?>" class="attachment-medium" alt="<?php $post->post_excerpt; ?>" /></a></p>
								<?php else : ?>
									<a href="<?php echo wp_get_attachment_url( $post->ID ); ?>" title="<?php echo esc_html( get_the_title( $post->ID ), 1 ); ?>" rel="attachment"><?php echo basename( $post->guid ); ?></a>
								<?php endif; ?>
								<div class="entry-caption">
									<?php if ( !empty( $post->post_excerpt ) ) the_excerpt(); ?>
								</div>
								<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
						<?php endwhile; endif; ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div><!-- /.wrapper -->

<?php get_footer(); ?>