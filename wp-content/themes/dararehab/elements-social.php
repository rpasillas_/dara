			<div class="social">
				<div>
					<span>
						<img src="<?php echo get_template_directory_uri(); ?>/img/social-phone.png" alt="Phone" />
						<table>
							<tr>
								<td>International:</td>
								<td><a href="tel:<?php echo ot_get_option( 'phone_int' ); ?>"><?php echo ot_get_option( 'phone_int' ); ?></a></td>
							</tr>
							<tr>
								<td>Toll-free from Australia:</td>
								<td><a href="tel:<?php echo ot_get_option( 'phone_au' ); ?>"><?php echo ot_get_option( 'phone_au' ); ?></a></td>
							</tr>
							<tr>
								<td>Toll-free from United Kingdom:</td>
								<td><?php echo do_shortcode( '[phone_uk]' ); ?></td>
							</tr>
							<tr>
								<td>USA &amp; Canada:</td>
								<td><?php echo do_shortcode( '[phone_us]' ); ?></td>
							</tr>
						</table>
						<a href="<?php echo get_page_link(38); ?>"><span class="glyphicon glyphicon-play"></span> All Other International Phone Numbers</a>
					</span>
				</div>
				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/img/social-facebook.png" alt="Facebook" />
					<span>
						<ul>
							<li><a href="http://www.facebook.com/<?php echo ot_get_option( 'social_facebook' ); ?>" target="_blank">Facebook</a></li>
							<li><a href="http://www.youtube.com/user/<?php echo ot_get_option( 'social_youtube' ); ?>" target="_blank">YouTube</a></li>
							<li><a href="http://www.twitter.com/<?php echo ot_get_option( 'social_twitter' ); ?>" target="_blank">Twitter</a></li>
							<li><a href="https://www.linkedin.com/company/<?php echo ot_get_option( 'social_linkedin' ); ?>" target="_blank">LinkedIn</a></li>
							<li><a href="https://plus.google.com/+<?php echo ot_get_option( 'social_googleplus' ); ?>/posts" target="_blank">Google+</a></li>
						</ul>
					</span>
				</div>
			</div><!--/.social-->