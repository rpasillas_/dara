<?php get_header(); ?>
			<div class="header" style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dot.png'), url('<?php echo get_template_directory_uri(); ?><?php banner_tree_img(); ?>') no-repeat center center / cover">
				<h1><?php echo get_the_title(11); ?></h1>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">

					<?php get_sidebar(); ?>

					<div class="col-lg-8 col-md-pull-4 text entry">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="date">
							<div class="month"><?php the_time('M ') ?></div>
							<div class="day"><?php the_time('j\<\s\u\p\>S\<\/\u\p\> ') ?></div>
							<div class="year"><?php the_time(' Y') ?> </div>
						</div>
						<div class="content">
							<?php get_template_part( 'entry' ); ?>
						</div>
						<?php endwhile; endif; ?>
						<?php get_template_part( 'nav', 'below' ); ?>
					</div>

				</div>
			</div>
		</div><!-- /.wrapper -->

<?php get_footer(); ?>