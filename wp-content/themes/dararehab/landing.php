<?php
/*
** Template Name: Landing Page
*/
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no">

		<title><?php wp_title( ' | ', true, 'right' ); ?></title>

		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/home.css">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/landing.css">

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery.fancybox.pack.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery.cycle2.min.js"></script>
		<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/js/responsiveslides.min.js"></script>-->
		<!--<script type="text/javascript">
			$(function () {
				$(".rslides").responsiveSlides({
					auto: true,
					pager: false,
					nav: true,
					speed: 500,
					timeout: 6000,
					namespace: "callbacks"
				});
			});
		</script>-->
		<script type="text/javascript">
			$('.cycle-overlay').show();
			$('.cycle-slideshow').mouseenter(function(){
				$('.cycle-overlay').show();
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.fancybox').fancybox({
					fitToView	: false,
					openEffect	: 'fade',
					closeEffect	: 'fade',
					centerOnScroll : 'true',
					scrollOutside : 'true'
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function (){
				$('iframe').each(function(){
					var url = $(this).attr("src");
					$(this).attr("src",url+"?wmode=transparent");
				});
			});
		</script>
		<?php echo ot_get_option( 'google_analytics_code' ); ?>
		<?php echo ot_get_option( 'google_remarketing_code' ); ?>
		<?php echo ot_get_option( 'infinity_code' ); ?>

		<?php wp_head(); ?>

	</head>

	<body id="home">

		<div id="wrap">

			<div class="hero">
				<div class="row">
					<div class="col-md-12">
						<div class="page-header">
							<img src="<?php echo get_template_directory_uri(); ?>/landing/img/logo.png" alt="DaraRehab.com" />
						</div>
						<p class="lead">Asia's premier and leading international destination for drug and alcohol rehabilitation, DARA (Drug &amp; Alcohol Rehab Asia) operates two world-class centers in Thailand.</p>
					</div>
				</div>
			</div>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<?php get_template_part('elements', 'social'); ?>
				<div class="container">
					<div class="text">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="photos">
				<div class="banner">
					<div class="container">
						<span>DARA Koh Chang</span>
						<span><a href="http://dararehab.com/koh-chang-photo-gallery/">View All Photos</a></span>
					</div>
				</div>
				<div class="container">
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-01" src="<?php echo get_template_directory_uri(); ?>/img/gallery-01.png" />
						<img class="gallery-02" src="<?php echo get_template_directory_uri(); ?>/img/gallery-02.png" />
						<img class="gallery-03" src="<?php echo get_template_directory_uri(); ?>/img/gallery-03.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-04" src="<?php echo get_template_directory_uri(); ?>/img/gallery-04.png" />
						<img class="gallery-05" src="<?php echo get_template_directory_uri(); ?>/img/gallery-05.png" />
						<img class="gallery-06" src="<?php echo get_template_directory_uri(); ?>/img/gallery-06.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-07" src="<?php echo get_template_directory_uri(); ?>/img/gallery-07.png" />
						<img class="gallery-08" src="<?php echo get_template_directory_uri(); ?>/img/gallery-08.png" />
						<img class="gallery-09" src="<?php echo get_template_directory_uri(); ?>/img/gallery-09.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-10" src="<?php echo get_template_directory_uri(); ?>/img/gallery-10.png" />
						<img class="gallery-11" src="<?php echo get_template_directory_uri(); ?>/img/gallery-11.png" />
						<img class="gallery-12" src="<?php echo get_template_directory_uri(); ?>/img/gallery-12.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-13" src="<?php echo get_template_directory_uri(); ?>/img/gallery-13.png" />
						<img class="gallery-14" src="<?php echo get_template_directory_uri(); ?>/img/gallery-14.png" />
						<img class="gallery-15" src="<?php echo get_template_directory_uri(); ?>/img/gallery-15.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-16" src="<?php echo get_template_directory_uri(); ?>/img/gallery-16.png" />
						<img class="gallery-17" src="<?php echo get_template_directory_uri(); ?>/img/gallery-17.png" />
						<img class="gallery-18" src="<?php echo get_template_directory_uri(); ?>/img/gallery-18.png" />
					</div>

				</div>
				<div class="banner">
					<div class="container">
						<span>DARA Chanthaburi</span>
						<span><a href="http://dararehab.com/chanthaburi-photo-gallery/">View All Photos</a></span>
					</div>
				</div>
				<div class="container">
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-01" src="/wp-content/uploads/2014/03/gallery-1.png" />
						<img class="gallery-02" src="/wp-content/uploads/2014/03/gallery-2.png" />
						<img class="gallery-03" src="/wp-content/uploads/2014/03/gallery-3.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-04" src="/wp-content/uploads/2014/03/gallery-4.png" />
						<img class="gallery-05" src="/wp-content/uploads/2014/03/gallery-5.png" />
						<img class="gallery-06" src="/wp-content/uploads/2014/03/gallery-6.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-07" src="/wp-content/uploads/2014/03/gallery-7.png" />
						<img class="gallery-08" src="/wp-content/uploads/2014/03/gallery-8.png" />
						<img class="gallery-09" src="/wp-content/uploads/2014/03/gallery-9.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-10" src="/wp-content/uploads/2014/03/gallery-101.png" />
						<img class="gallery-11" src="/wp-content/uploads/2014/03/gallery-111.png" />
						<img class="gallery-12" src="/wp-content/uploads/2014/03/gallery-121.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-13" src="/wp-content/uploads/2014/03/gallery-131.png" />
						<img class="gallery-14" src="/wp-content/uploads/2014/03/gallery-141.png" />
						<img class="gallery-15" src="/wp-content/uploads/2014/03/gallery-151.png" />
					</div>
					<div class="cycle-slideshow" data-cycle-pause-on-hover="true">
						<img class="gallery-16" src="/wp-content/uploads/2014/03/gallery-161.png" />
						<img class="gallery-17" src="/wp-content/uploads/2014/03/gallery-171.png" />
						<img class="gallery-18" src="/wp-content/uploads/2014/03/gallery-181.png" />
					</div>
				</div>
			</div><!-- /Photo Gallery 2 -->

			<div class="map">
				<div class="container">
					<div class="callout">
						<span>Map</span>
						<p>Asia’s first and leading international destination for drug and alcohol rehabilitation, DARA Drug &amp; Alcohol Rehab Asia, operates two world-class centers in Thailand. </p>
					</div>
					<ul class="tacks">
						<li>
							<a href="<?php echo get_page_link(49); ?>">Chanthaburi</a>
							<div>
								<div>
									<span>Chanthaburi</span>
									<span>"City of the Moon"</span>
									<a href="<?php echo get_page_link(49); ?>" class="button">Learn More</a>
								</div>
							</div>
						</li>
						<li>
							<a href="<?php echo get_page_link(48); ?>">Koh Chang</a>
							<div>
								<div>
									<span>Koh Chang</span>
									<span>"Elephant Island"</span>
									<a href="<?php echo get_page_link(48); ?>" class="button">Learn More</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div><!-- /Map -->

			<div class="content">
				<div class="container">
					<?php //echo get_post_meta($post->ID, 'secondary', true); ?>
					<?php echo the_field('secondary'); ?>
				</div>
			</div><!-- /Content -->
		</div><!-- /.wrapper -->
		<div class="footer">
			<div class="top">
				<div class="container">
				<?php if(is_front_page() || is_page('1357')) { ?>
					<div class="text">
							<span>Ready for a change?</span>
							<p class="bold">Help is available 24 hours a day / 7 days a week</p>
					</div>
					<div class="button-contact">
						<span class="desktop"><a href="#form" type="button" class="button fancybox inline">Contact Us Now</a></span>
						<span class="mobile"><a href="/contact" type="button" class="button">Contact Us Now</a></span>
					</div>
				<?php } else { ?>
					<div class="text">
							<span>Contact Form</span>
							<?php echo do_shortcode( '[contact-form-7 id="584" title="Footer Contact Form"]' ); ?>
					</div>
				<?php } ?>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					
					<div class="col-lg-3">
						<span class="headline">A Member Of</span>
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-onaatp.png" alt="ONAATP Logo" />
						<?php wp_nav_menu( array( 'theme_location' => 'footer-left', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-lg-3">
						<span class="headline">In Association With</span>
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-bangkokhospital.png" alt="Bangkok Hospital Logo" />
						<?php wp_nav_menu( array( 'theme_location' => 'footer-center', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-lg-3">
						<span class="headline">Get Connected</span>
						<ul class="social">
							<li><a href="http://www.facebook.com/<?php echo ot_get_option( 'social_facebook' ); ?>" target="_blank">Facebook</a></li>
							<li><a href="http://www.twitter.com/<?php echo ot_get_option( 'social_twitter' ); ?>" target="_blank">Twitter</a></li>
							<li><a href="http://www.youtube.com/user/<?php echo ot_get_option( 'social_youtube' ); ?>" target="_blank">YouTube</a></li>
							<li><a href="mailto:info@dararehab.com">Email</a></li>
						</ul>
						<span class="headline">Accepted Insurance</span>
						<div class="accepted-insurance">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-cigna.png" width="35" alt="Cigna">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-geoblue-rvsd.png" width="88" alt="Geo Blue">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-hth.png" width="104" alt="HTH Worldwide">
						</div>
					</div>
					<div class="col-lg-3">
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png" alt="Dara Thailand">
					</div>
					<div class="copyline">
						<span>Email <a href="mailto:<?php echo ot_get_option( 'email' ); ?>"><?php echo ot_get_option( 'email' ); ?></a> / Copyright &copy; 2008-2014. All Rights Reserved. / <a href="<?php echo get_permalink( '142' ); ?>">Privacy Policy</a> · <a href="<?php echo get_permalink( '143' ); ?>">Terms of Use</a> · <a href="<?php echo get_permalink( '144' ); ?>">Disclaimer</a></span>
					</div>
				</div>
			</div>
			<div id="form" class="noshow">
				<p><span class="cta giant">Contact Form</span></p>
				<?php echo do_shortcode( '[contact-form-7 id="7" title="Contact Form"]' ); ?>
			</div>
		</div>

		<script src="<?php echo get_template_directory_uri(); ?>/lib/js/bootstrap.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/lib/js/jquery.slicknav.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#mobile').slicknav();
			});
		</script>

		<!-- begin SnapEngage code -->
		<script type="text/javascript">
		  (function() {
		    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
		    se.src = '//commondatastorage.googleapis.com/code.snapengage.com/js/a5eec884-f318-4ab3-82e6-9a0cf29829d6.js';
		    var done = false;
		    se.onload = se.onreadystatechange = function() {
		      if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
		        done = true;
		        // Place your SnapEngage JS API code below
			var seAgent;
			SnapABug.setCallback('StartChat', function(email, msg, type) {
			_gaq.push(['_trackEvent', 'Live Chat', 'started', seAgent]);
			});

			SnapABug.setCallback('ChatMessageSent', function(msg) {
			_gaq.push(['_trackEvent', 'Live Chat', 'message sent', seAgent]);
			});
		        // SnapEngage.allowChatSound(true); // Example JS API: Enable sounds for Visitors.
		      }
		    };
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
		  })();
		</script>
		<!-- end SnapEngage code -->
		<?php wp_footer(); ?>
	</body>
</html>