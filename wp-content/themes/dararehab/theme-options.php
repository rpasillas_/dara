<?php
/**
 * Initialize the options before anything else.
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array.
   */
  $saved_settings = get_option( 'option_tree_settings', array() );

  /**
   * Custom settings array that will eventually be
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'sidebar'       => ''
    ),
    'sections'        => array(
      array(
        'id'          => 'main_settings',
        'title'       => 'Main Settings'
      ),
      array(
        'id'          => 'ppc_settings',
        'title'       => 'PPC Settings'
      ),
      array(
        'id'          => 'social_settings',
        'title'       => 'Social Media Settings'
      ),
      array(
        'id'          => 'homepage_slider',
        'title'       => 'Homepage Slider'
      )
    ),
    'settings'        => array(
      array(
        'id'          => 'email',
        'label'       => 'Email Address',
        'desc'        => 'Enter the main email address for the site.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_us',
        'label'       => 'Phone Number: United States & Canada',
        'desc'        => 'Enter the USA/Canada phone number for the site.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_uk',
        'label'       => 'Phone Number: United Kingdom',
        'desc'        => 'Enter the UK phone number for the site.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_au',
        'label'       => 'Phone Number: Australia',
        'desc'        => 'Enter the Australia phone number for the site.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_int',
        'label'       => 'Phone Number: International',
        'desc'        => 'Enter the international phone number for the site.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'cta',
        'label'       => 'Call To Action',
        'desc'        => 'Enter the messaging for the call to action.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'main_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),

      array(
        'id'          => 'google_analytics_code',
        'label'       => 'Google Analytics Code',
        'desc'        => 'This is the area to enter the client\'s Google Analytics code.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'ppc_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'google_remarketing_code',
        'label'       => 'Google Remarketing Code',
        'desc'        => 'This is the area to enter the client\'s Google Remarketing code.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'ppc_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'infinity_code',
        'label'       => 'Infinity Code',
        'desc'        => 'This is the area to display the client\'s Infinity tracking code.',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'ppc_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_span_us',
        'label'       => 'Infinity Phone Number Span Class: United States',
        'desc'        => 'Enter the phone number\'s USA-related Infinity span class.',
        'std'         => '123456789',
        'type'        => 'text',
        'section'     => 'ppc_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'phone_span_uk',
        'label'       => 'Infinity Phone Number Span Class: United Kingdom',
        'desc'        => 'Enter the phone number\'s UK-based Infinity span class.',
        'std'         => '123456789',
        'type'        => 'text',
        'section'     => 'ppc_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'social_facebook',
        'label'       => 'Facebook ID',
        'desc'        => 'Enter the Facebook ID.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'social_twitter',
        'label'       => 'Twitter Handle',
        'desc'        => 'Enter the Twitter handle.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'social_youtube',
        'label'       => 'YouTube ID',
        'desc'        => 'Enter the YouTube ID.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'social_linkedin',
        'label'       => 'LinkedIn ID',
        'desc'        => 'Enter the LinkedIn ID.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'social_googleplus',
        'label'       => 'Google+ ID',
        'desc'        => 'Enter the Google+ ID.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social_settings',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_0',
        'label'       => 'Slide #0 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
 array(
        'id'          => 'slider_text_0',
        'label'       => 'Slide #0 Text',
        'desc'        => 'Enter the text to support feature image 0',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_1',
        'label'       => 'Slide #1 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_text_1',
        'label'       => 'Slide #1 Text',
        'desc'        => 'Enter the text to support feature image 1',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_2',
        'label'       => 'Slide #2 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_text_2',
        'label'       => 'Slide #2 Text',
        'desc'        => 'Enter the text to support feature image 2',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_3',
        'label'       => 'Slide #3 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
 array(
        'id'          => 'slider_text_3',
        'label'       => 'Slide #3 Text',
        'desc'        => 'Enter the text to support feature image 3',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_4',
        'label'       => 'Slide #4 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
 array(
        'id'          => 'slider_text_4',
        'label'       => 'Slide #4 Text',
        'desc'        => 'Enter the text to support feature image 4',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
      array(
        'id'          => 'slider_image_5',
        'label'       => 'Slide #5 Image',
        'desc'        => 'Upload image for the home page feature panel. Optimum sizes for this panel are 960px x 410px',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
 array(
        'id'          => 'slider_text_5',
        'label'       => 'Slide #5 Text',
        'desc'        => 'Enter the text to support feature image 4',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'homepage_slider',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      )
    )
  );

  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );

  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings );
  }

}