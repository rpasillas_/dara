<?php get_header(); ?>
			<div class="header">
				<h1><?php printf( __( 'Search Results for: <em>%s</em>' ), get_search_query() ); ?></h1>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">

					<?php get_sidebar(); ?>
					
					<div class="col-lg-8 col-md-pull-4 text">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p class="breadcrumbs"><a href="' . home_url() . '"><span class="glyphicon glyphicon-home"></span></a>','</p>');
						} ?>
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'entry' ); ?>
							<?php endwhile; ?>
							<?php get_template_part( 'nav', 'below' ); ?>
							<?php else : ?>
								<h2><?php _e( 'Nothing Found' ); ?></h2>
								<p><?php _e( 'We\'re sorry, but nothing matched your search. Please try again.' ); ?></p>
								<?php get_search_form(); ?>
						<?php endif; ?>
					</div>
					
				</div>
			</div>
		</div><!-- /.wrapper -->

<?php get_footer(); ?>