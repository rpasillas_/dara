<?php get_header(); ?>
			<div class="header">
				<h1>Page Not Found</h1>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">

					<?php get_sidebar(); ?>

					<div class="col-lg-8 col-md-pull-4 text">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p class="breadcrumbs"><a href="' . home_url() . '"><span class="glyphicon glyphicon-home"></span></a>','</p>');
						} ?>
						<p>Nothing found for the requested page. Try a search instead.</p>
						<?php get_search_form(); ?>
					</div>
					
				</div>
			</div>
		</div><!-- /.wrapper -->

<?php get_footer(); ?>