<?php

//-----[ Panel: Wide ]-----\\
function sc_panel($atts, $content = null) {
	extract( shortcode_atts(
		array(
			'title' => 'panel',
		),
		$atts ) );
	return '
						<div class="panel panel-default">
							<div class="panel-heading">' . esc_attr($title) . '</div>
							<div class="panel-body">
								<p>' . $content . '</p>
							</div>
						</div>';
}
add_shortcode('panel', 'sc_panel');


//-----[ Panel: Left ]-----\\
function sc_panel_left($atts, $content = null) {
	extract( shortcode_atts(
		array(
			'title' => 'panel_left',
		),
		$atts ) );
	return '
						<div class="panel panel-default left">
							<div class="panel-heading">' . esc_attr($title) . '</div>
							<div class="panel-body">
								<p>' . $content . '</p>
							</div>
						</div>';
}
add_shortcode('panel_left', 'sc_panel_left');


//-----[ Panel: Right ]-----\\
function sc_panel_right($atts, $content = null) {
	extract( shortcode_atts(
		array(
			'title' => 'panel_right',
		),
		$atts ) );
	return '
						<div class="panel panel-default right">
							<div class="panel-heading">' . esc_attr($title) . '</div>
							<div class="panel-body">
								<p>' . $content . '</p>
							</div>
						</div>';
}
add_shortcode('panel_right', 'sc_panel_right');


//-----[ Wide Captioned Image ]-----\\
function sc_widecap($atts, $content = null) {
	extract( shortcode_atts(
		array(
			'caption' => 'widecap',
		),
		$atts ) );
	return '
						<div class="imgcap">
							' . $content . '
							<div class="caption">
								<p>' . esc_attr($caption). '</p>
							</div>
						</div>';
}
add_shortcode('widecap', 'sc_widecap');


//-----[ Pullquote ]-----\\
function sc_pullquote($atts, $content = null) {
	return '
						<span class="pullquote">' . $content . '</span>';
}
add_shortcode('pullquote', 'sc_pullquote');


//-----[ Double-Column List ]-----\\
function sc_twocol($atts, $content = null) {
	return '
						<div class="ul-double">' . $content . '</div>';
}
add_shortcode('twocol', 'sc_twocol');


//-----[ CTA: Giant ]-----\\
function sc_cta_giant($atts, $content = null) {
	return '
						<span class="cta giant">' . $content . '</span>';
}
add_shortcode('cta_giant', 'sc_cta_giant');


//-----[ CTA: Large ]-----\\
function sc_cta_large($atts, $content = null) {
	return '
						<span class="cta large">' . $content . '</span>';
}
add_shortcode('cta_large', 'sc_cta_large');


//-----[ CTA: Medium ]-----\\
function sc_cta_medium($atts, $content = null) {
	return '
						<span class="cta medium">' . $content . '</span>';
}
add_shortcode('cta_medium', 'sc_cta_medium');


//-----[ CTA: Small ]-----\\
function sc_cta_small($atts, $content = null) {
	return '
						<span class="cta small">' . $content . '</span>';
}
add_shortcode('cta_small', 'sc_cta_small');


//-----[ CTA: Mini ]-----\\
function sc_cta_mini($atts, $content = null) {
	return '
						<span class="cta mini">' . $content . '</span>';
}
add_shortcode('cta_mini', 'sc_cta_mini');

//-----[ Phone Numbers ]-----\\
function sc_phone_us($atts, $content = null) {
	$span  = ot_get_option( 'phone_span_us' );
	$num   = ot_get_option( 'phone_us' );
	$rid   = array('-','(',')',' ');
	$clean = (str_replace($rid,'',$num));
	if ($span) {
		return '<span class="' . $span . ' clickable us-num">' . $num . '</span>';
	} else if (strpos($num,'+') !== 'false') {
		return '<a href="tel:' . $clean . '" class="us-num">' . $num . '</a>';
	} else {
		return '<a href="tel:+' . $clean . '" class="us-num">' . $num . '</a>';
	}
}
add_shortcode('phone_us', 'sc_phone_us');

function sc_phone_uk($atts, $content = null) {
	$span  = ot_get_option( 'phone_span_uk' );
	$num   = ot_get_option( 'phone_uk' );
	$rid   = array('-','(',')',' ');
	$clean = (str_replace($rid,'',$num));
	if ($span) {
		return '<span class="' . $span . ' clickable uk-num">' . $num . '</span>';
	} else if (strpos($num,'+') !== 'false') {
		return '<a href="tel:' . $clean . '" class="uk-num">' . $num . '</a>';
	} else {
		return '<a href="tel:+' . $clean . '" class="uk-num">' . $num . '</a>';
	}
}
add_shortcode('phone_uk', 'sc_phone_uk');

function sc_phone_au($atts, $content = null) {
	return ot_get_option( 'phone_au' );
}
add_shortcode('phone_au', 'sc_phone_au');

function sc_phone_int($atts, $content = null) {
	return ot_get_option( 'phone_int' );
}
add_shortcode('phone_int', 'sc_phone_int');

function sc_email($atts, $content = null) {
	return ot_get_option( 'email' );
}
add_shortcode('email', 'sc_email');


function geo_phone($atts, $content = null){
	$country = do_shortcode('[geoip-country]');
	if($country=='US' || $country=='CA'){
		$phone = do_shortcode('[phone_us]');
	}else if($country == 'GB'){
		$phone = do_shortcode('[phone_uk]');
	}else if($country == 'AU'){
		$phone = '<a href="tel:' . ot_get_option( 'phone_au' ) . '"  class="au-num">' . ot_get_option( 'phone_au' ) . '</a>';
	}else{
		$phone = '<a href="tel:' . ot_get_option( 'phone_int' ) . '"  class="au-num">' . ot_get_option( 'phone_int' ) . '</a>';
	}
	
	return $phone;
}
add_shortcode('geo-phone','geo_phone');




//[ 21 ]----- Candybar
	//http://stackoverflow.com/questions/12325348/calling-wordpress-get-template-part-from-insde-a-shortcode-function-renders-temp
function candybar($atts){	
		ob_start();	
		get_template_part('part-candybar');
		$data = ob_get_contents();
		ob_end_clean();
		return $data;
}
add_shortcode('cta-banner', 'candybar');