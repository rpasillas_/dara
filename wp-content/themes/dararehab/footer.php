		<div class="footer">
			<div class="top">
				<div class="container">
				<?php if(is_front_page() || is_page('1357')) { ?>
					<div class="text">
							<span>Ready for a change?</span>
							<p class="bold">Help is available 24 hours a day / 7 days a week</p>
					</div>
					<div class="button-contact">
						<span class="desktop"><a href="#form" type="button" class="button fancybox inline">Contact Us Now</a></span>
						<span class="mobile"><a href="/contact" type="button" class="button">Contact Us Now</a></span>
					</div>
				<?php } else { ?>
					<div class="text">
							<span>Contact Form</span>
							<?php echo do_shortcode( '[contact-form-7 id="584" title="Footer Contact Form"]' ); ?>
					</div>
				<?php } ?>
				</div>
			</div>
			<div class="bottom">
				<div class="container">
					
					<div class="col-lg-3">
						<span class="headline">A Member Of</span>
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-onaatp.png" alt="ONAATP Logo" />
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo-bacp.png" alt="British Association for Counselling & Psychotherapy">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-left', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-lg-3">
						<span class="headline">In Association With</span>
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-bangkokhospital.png" alt="Bangkok Hospital Logo" />
						<?php wp_nav_menu( array( 'theme_location' => 'footer-center', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
					</div>
					<div class="col-lg-3">
						<span class="headline">Get Connected</span>
						<ul class="social">
							<li><a href="http://www.facebook.com/<?php echo ot_get_option( 'social_facebook' ); ?>" target="_blank">Facebook</a></li>
							<li><a href="http://www.twitter.com/<?php echo ot_get_option( 'social_twitter' ); ?>" target="_blank">Twitter</a></li>
							<li><a href="http://www.youtube.com/user/<?php echo ot_get_option( 'social_youtube' ); ?>" target="_blank">YouTube</a></li>
							
							<li><a href="http://www.plus.google.com/<?php echo ot_get_option( 'social_googleplus' ); ?>/posts" target="_blank">LinkedIn</a></li>
							<li><a href="http://www.linkedin.com/company/<?php echo ot_get_option( 'social_linkedin' ); ?>" target="_blank">Google+</a></li>
							<li><a href="mailto:info@dararehab.com">Email</a></li>
						</ul>
						<span class="headline">Accepted Insurance</span>
						<div class="accepted-insurance">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-cigna.png" width="35" alt="Cigna">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-geoblue-rvsd.png" width="88" alt="Geo Blue">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-hth.png" width="104" alt="HTH Worldwide">
						</div>
						<div class="">
							<img style="margin-right: 10px;" src="<?php echo get_template_directory_uri(); ?>/img/assistance.png" width="115" alt="AXA Assistance">
							<img src="<?php echo get_template_directory_uri(); ?>/img/international_sos.png" width="62" alt="International SOS">
						</div>
					</div>
					<div class="col-lg-3">
						<img src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png" alt="Dara Thailand">
					</div>
					<div class="copyline">
						<span>Email <a href="mailto:<?php echo ot_get_option( 'email' ); ?>"><?php echo ot_get_option( 'email' ); ?></a> / Copyright &copy; 2008-2014. All Rights Reserved. / <a href="<?php echo get_permalink( '142' ); ?>">Privacy Policy</a> · <a href="<?php echo get_permalink( '143' ); ?>">Terms of Use</a> · <a href="<?php echo get_permalink( '144' ); ?>">Disclaimer</a></span>
					</div>
				</div>
			</div>
			<div id="form" class="noshow">
				<p><span class="cta giant">Contact Form</span></p>
				<?php echo do_shortcode( '[contact-form-7 id="7" title="Contact Form"]' ); ?>
			</div>
		</div>

		<script src="<?php echo get_template_directory_uri(); ?>/lib/js/bootstrap.min.js"></script>

		<!-- begin SnapEngage code -->
		<script type="text/javascript">
		  (function() {
		    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
		    se.src = '//commondatastorage.googleapis.com/code.snapengage.com/js/a5eec884-f318-4ab3-82e6-9a0cf29829d6.js';
		    var done = false;
		    se.onload = se.onreadystatechange = function() {
		      if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
		        done = true;
		       
				var seAgent;
				SnapABug.setCallback('StartChat', function(email, msg, type) {
					_gaq.push(['_trackEvent', 'Live Chat', 'started', seAgent]);
				});

				SnapABug.setCallback('ChatMessageSent', function(msg) {
					_gaq.push(['_trackEvent', 'Live Chat', 'message sent', seAgent]);
				});

		      }
		    };
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
		  })();
		</script>
		<!-- end SnapEngage code -->
		<?php wp_footer(); ?>
	</body>
</html>