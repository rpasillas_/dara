					<div class="col-lg-4 col-lg-push-8 text sidebar">
						<?php
							if ( is_page() ) {
								dynamic_sidebar( 'interior' );
							} else {
								dynamic_sidebar( 'blog' );
							}
						?>
					</div>