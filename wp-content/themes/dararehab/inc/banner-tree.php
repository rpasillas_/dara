<?php

	function is_tree($pid) { //$pid = The ID of the ancestor page
		global $post;
		$anc = get_post_ancestors( $post->ID );
		foreach($anc as $ancestor) {
			if(is_page() && $ancestor == $pid) {
				return true;
			}
		}
		if ( is_page() && (is_page($pid)) ) {
			return true;
		} else {
			return false;
		}
	}

	function get_top_parent_page_id() {
		global $post;
		$ancestors = $post->ancestors;
		if ($ancestors) {
			return end($ancestors);
		} else {
			return $post->ID;
		}
	}

	function banner_tree_img() {
		$parent_page = get_top_parent_page_id($post->ID);
		if ( is_home() || is_single() || is_archive() ) {
			echo '/img/banner-blog.png';
		} else if ( $parent_page == 14 ) {
			echo '/img/banner-programs.png';
		} else if ( $parent_page == 18 ) {
			echo '/img/banner-center.png';
		} else if ( $parent_page == 22 ) {
			echo '/img/banner-works.png';
		} else if ( $parent_page == 41 ) {
			echo '/img/banner-why.png';
		} else if ( $parent_page == 38 ) {
			echo '/img/banner-contact.png';
		} else {
			echo '/img/banner-default.png';
		}
	}