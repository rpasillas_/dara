<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="header" style="background: url('<?php echo get_template_directory_uri(); ?>/img/overlay-dot.png'), url('<?php echo get_template_directory_uri(); ?><?php banner_tree_img(); ?>') no-repeat center center / cover">
				<span class="page-title"><?php echo get_the_title(11); ?></span>
			</div>
			<?php get_template_part('elements', 'social'); ?>
			<?php get_template_part('elements', 'contactbar'); ?>
			<div class="main">
				<div class="container">

					<?php get_sidebar(); ?>

					<div class="col-lg-8 col-md-pull-4 text">
						<?php if ( !is_single( '164' ) ) {
						    if ( function_exists('yoast_breadcrumb') ) {
							    yoast_breadcrumb('<p class="breadcrumbs"><span class="glyphicon glyphicon-home"></span>','</p>');
						    }
						} ?>
						<?php get_template_part( 'entry' ); ?>
						<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
					</div>
					
				</div>
			</div>
		</div><!-- /.wrapper -->

<?php endwhile; endif;
get_footer(); ?>